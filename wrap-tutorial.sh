#!/bin/bash

cp 01-sales-report.html 01-sales-report-reference.html
cp 02-sales-report.html 02-sales-report-reference.html

ZIPFILE=automated-reporting-tutorial.zip

rm -f $ZIPFILE

zip $ZIPFILE \
  0?-sales-report-template.R \
  0?-sales-report-reference.html \
  automated-reporting-tutorial.html \
  r-markdown-template.Rmd \
  data/widget-sales-subset.xlsx \
  data/widget-sales-summary.xlsx \
  data/widget-sales.xlsx

rm -f 0?-sales-report-reference.html