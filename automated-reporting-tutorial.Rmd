---
title: "Automated Reporting with R (Tutorial)"
subtitle: "satRday (Paris)"
author: Andrew Collier
date:  "23 February 2019"
output:
  html_document:
    theme: cosmo
    highlight: tango
    toc: true
    toc_float: true
    code_folding: hide
    df_print: paged
    css: styles.css
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(fontawesome)
```

<table>
<tr><td>`r fa("envelope", fill = "steelblue")`&nbsp;</td><td>andrew@exegetic.biz</td></tr>
<tr><td>`r fa("linkedin", fill = "steelblue")`&nbsp;</td><td>https://www.linkedin.com/in/datawookie/</td></tr>
<tr><td>`r fa("twitter", fill = "steelblue")`&nbsp;</td><td><a href="https://twitter.com/datawookie">@datawookie</a></td></tr>
</table>

![](https://pbs.twimg.com/profile_images/1093807064709771269/V6YVGA_H_400x400.jpg)

satRday is in Johannesburg on 6 April 2019. More details at https://joburg2019.satrdays.org/.

- Follow us at <a href="https://twitter.com/satRday_ZAF">@satRday_ZAF</a>.
- Submit a talk before 11 March 2019.

# Introduction

This tutorial provides a starting point for generating automated reports using R. It assumes some previous knowledge of R and some exposure to R Markdown.

<div class="alert alert-info">
This tutorial is for you if

- you know a bit of R;
- you might (or might not) be familiar with R Markdown; and
- you're interested in using R to generate reports (or other automated documents).

This tutorial is *not* for you if

- you're an R Markdown wizard/ninja/deity.
</div>

## Setup

Install the following packages:

- [`tidyverse`](https://cran.r-project.org/web/packages/tidyverse/)
- [`knitr`](https://cran.r-project.org/web/packages/knitr/)
- [`kableExtra`](https://cran.r-project.org/web/packages/kableExtra/)
- [`scales`](https://cran.r-project.org/web/packages/scales/)
- [`hrbrthemes`](https://cran.r-project.org/web/packages/hrbrthemes/)
- [`countrycode`](https://cran.r-project.org/web/packages/countrycode/).
<!--
- [`mailR`](https://cran.r-project.org/web/packages/mailR/)
- [`rvest`](https://cran.r-project.org/web/packages/rvest/)
- [`glue`](https://cran.r-project.org/web/packages/glue/)
- [`emo`](https://github.com/hadley/emo)
- [`plotly`](https://cran.r-project.org/web/packages/plotly/)
- [`rtweet`](https://cran.r-project.org/web/packages/rtweet/)
- [`telegram`](https://cran.r-project.org/web/packages/telegram/) and
- [`slackr`](https://cran.r-project.org/web/packages/slackr/).
-->

<!--
You'll also need:

- a [Google Mail]() account
- a [Twitter]() account
- (optional) [Slack](https://slack.com/) installed somewhere and
- (optional) [Telegram](https://telegram.org/) installed on your mobile device.
-->

## The Mouse in the Room

Many analysts do repetitive manual tasks on a similar set of data every day, week or month. They do these analyses in Excel and then copy-paste the updated pivot tables and figures into Word or PowerPoint reports, then forward to various stakeholders.

![](fig/old-school.png)

An Old School reporting workflow might look something like this:

- *manually* load data into Excel;
- *manually* prepare data (merging and pivoting);
- *manually* prepare figures;
- *manually* create a Word or PowerPoint document;
- *manually* copy/paste figures from Excel;
- *manually* copy/paste tables from Excel;
- *manually* tweak formatting of figures and tables;
- *manually* add document title, section headings, logo etc.;
- *manually* export to PDF and
- *manually* create email and attach PDF.

Notice how every step is done "manually"?

The origin of the word "manual" is "manus", Latin for "hand". So each step is literally performed "by hand".

More accurately though, we should say that each step is done "by mouse".

![](fig/mouse.png)

The mouse makes things easier in the short term.

But it reduces your capacity to automate. And that's going to hurt you in the long term.

Before the invention of the mouse (around 1964), computer instructions were typed into a terminal (or, even earlier, recorded using punched cards and tape). Today we generally interact with a computer by jiggling a mouse and clicking on things.

<blockquote>
There's a rat in me kitchen what am I gonna do?<br>
There's a rat in me kitchen what am I gonna go?<br> 
I'm gonna fix that rat that's what I'm gonna do,<br>
I'm gonna fix that rat.<br>
<div style="text-align: right">--- UB40, <a href="https://en.wikipedia.org/wiki/Rat_in_Mi_Kitchen"><em>Rat in Mi Kitchen</em></a>, 1986</div>
</blockquote>

The **bad** news is that the manual process is mundane and prone to errors. Point-and-click interactions are not reproducible. Excel spreadsheets are notoriously fragile. Humans get bored and lose concentration: errors creep in.

![](fig/spreadsheets-fragile.png)

The **good** news is that these manual processes are (usually!) easy to automate. We have a *fix* for that mouse!

## Why?

Automation will

- save time (reclaim your lunch hour!) and
- reduce errors (reports are consistent and reliable).

![](fig/reclaim-lunch-hour.png)

New data? Run the report.

Updated data? Run the report.

Report required every Monday at 09:00? No problem, just add it to the scheduler.

## What?

Ideally you want to automate the entire pipeline from data ingestion through to report dissemination.

Everything should just happen with the metaphorical click of a <span type="button" class="btn btn-success">button</span>.

## When?

A rule of thumb: <div class="alert alert-warning">If you've repeated a process three times, then it's time to automate.</div>

Why wait? Because there's an up front cost. You'll only amortise that cost if you will be repeating the process many times in the future.

![xkcd: [Automation](https://xkcd.com/1319/). It's a little hyperbolic, but there's truth too.](fig/xkcd-1319-automation.png)

## How?

Automation is about building recipes. We'll be using R to build and run those recipes.

I'm assuming that everybody knows about R, but just in case...

[R](https://www.r-project.org/) is a high level language for statistical computing. It has _literally_ thousands of libraries which allow it to accomplish a very broad range of tasks.

These are the components of a reporting workflow using R:

- an R Markdown document (defines content of the report);
- one or more sources of data (might be CSV, XLSX, a database or web scraping); and
- (optional) a script to deliver the report to one of more channels.

![](fig/workflow-new.svg)


### Plain Markdown and R Markdown

[Markdown](https://en.wikipedia.org/wiki/Markdown) is a lightweight language for text formatting. It's simple but expressive. For clarity, we'll refer to this as "Plain Markdown".

[R Markdown](http://rmarkdown.rstudio.com/) is a superset of Plain Markdown. It has a few extra bells and whistles.

With R Markdown it's possible to produce documents which:

- include code (and output from that code);
- render to a variety of formats including:

    - HTML
    - Markdown
    - PDF
    - Microsoft Word or OpenDocument
    - RTF
    - various HTML or PDF presentation formats and
    - dashboards.

An excellent resource is the [R Markdown Cheat Sheet](https://github.com/rstudio/cheatsheets/blob/master/rmarkdown-2.0.pdf).

<div class="alert alert-danger">Look at R Markdown template document, `r-markdown-template.Rmd`.</div>

![](fig/r-markdown-workflow.svg)

Build an R Markdown document with the following steps:

1. Create an R Markdown `.Rmd` file comprised of text, images and code chunks.
2. Knit the `.Rmd` file using [knitr](https://github.com/yihui/knitr), which will execute code chunks and produce a Plain Markdown `.md`
3. Render the `.md` to a variety of formats using [pandoc](https://pandoc.org/), a universal document converter.

All of this is handled transparently within RStudio.

### Data Import and Wrangling

The import and preparation of data for reporting is often tedious and time consuming.

R can really help, especially with the following tasks:

- reading data from spreadsheets
- consolidating data from multiple tabs
- adding, removing and manipulating columns
- filtering and sorting rows and
- merging data (equivalent of a SQL `JOIN` or Excel `VLOOKUP`).

We'll be leveraging a few libraries, notably `dplyr`, `tidyr` and `purrr`, to assist with the wrangling process.

# Tutorial

## Simple Sales Report

We'll start by generating a simple report which will document the total number of units sold for a selection of products.

Specifically we're going to

1. load the data
2. create a table
3. dynamically generate a timestamp string and
4. plot a simple chart.

We'll start by fleshing out a `.R` script then transfer that across to a `.Rmd` file to build the report.

## Improved Sales Report

Now we're going to build upon the ideas from the previous report by

- handling more challenging data
- doing more wrangling
- creating a more attractive table and
- producing some useful plots.

We're going to do this in such a way that the report generalises well to new data.

# Epilogue

I hope that you have found this useful. Lots of our clients find that Automated Reporting makes an enormous difference to the way that they are able to run their organisations.

Other fun things that are useful to incorporate into an Automated Reporting workflow:

- Twitter
- Slack and
- Telegram.

All of these are possible from within R and we have training material to show how it's done.

## Session Information

Here are the details of the system used to generate this document.

```{r}
sessionInfo()
```
